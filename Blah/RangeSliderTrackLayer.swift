//
//  RangeSliderTrackLayer.swift
//  Blah
//
//  Created by Jatin Garg on 21/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import QuartzCore

class RangeSliderTrackLayer: CALayer {
    weak var rangeSlider: RangeSlider?
    
    override func draw(in ctx: CGContext) {
        guard let slider = rangeSlider
            else{
                return
        }
        
        //clip the track
        let cornerRadius = bounds.height * slider.curvaceousness / 2
        let path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        ctx.addPath(path.cgPath)
        
        //fill the track
        ctx.setFillColor(slider.trackTintColor.cgColor)
        ctx.addPath(path.cgPath)
        ctx.fillPath()
        
        //fill the highlighted Range
        ctx.setFillColor(slider.trackHighLightTintColor.cgColor)
        let lowerValuePosition = CGFloat(slider.positionForValue(value: Double(slider.lowerValue)))
        let upperValuePosition = CGFloat(slider.positionForValue(value: Double(slider.upperValue)))
        let rect = CGRect(x: lowerValuePosition, y: 0.0, width: upperValuePosition - lowerValuePosition, height: bounds.height)
        ctx.fill(rect)
        
        
    }
}
