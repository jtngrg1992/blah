//
//  HotelCatalogCell.swift
//  Blah
//
//  Created by Jlabs on 10/12/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import Cosmos

class HotelCatalogCell: UITableViewCell {

    
    @IBOutlet weak var displayImage: UIImageView!
    @IBOutlet weak var hotelTitle: UILabel!
    @IBOutlet weak var hotelAddress: UILabel!
    @IBOutlet weak var hotelStartRating: CosmosView!
    @IBOutlet weak var hotelRating: UILabel!
    @IBOutlet weak var reviewsGateway: UILabel!
    @IBOutlet weak var secondaryContainer: UIView!
    @IBOutlet weak var bookBtn: UIButton!
    @IBOutlet weak var overlayView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        secondaryContainer.layer.masksToBounds = true
        secondaryContainer.layer.cornerRadius = 3
        contentView.backgroundColor = UIColor.clear
        overlayView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
