//
//  CityCodes.swift
//  Blah
//
//  Created by Jlabs on 10/6/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import Foundation

struct CityCodes{
    let codes : [Int: [String : String]] = [0:["Delhi-NCR" : "delhi"] ,1:["Mumbai" : "mumbai"], 2:["Rajasthan" : "rajasthan"]]
}

struct Hosts{
    let hostName = "bestin.herokuapp.com"
    //let hostName = "localhost:4000"
}
struct APIEndPoints{
    let fetchHotelsByCity = { (city: String) -> String in
        let token = DataModal.sharedInstance.token!
        let client = UIDevice.current.identifierForVendor!.uuidString
        return "https://\(Hosts().hostName)/hotels?page=1&city=\(city)&access_token=\(token)&client_id=\(client)"
    }
    
    let getAccessToken = {(deviceID : String) ->String in
        return "https://\(Hosts().hostName)/getToken?device=\(deviceID)"
    }
}

extension UIViewController{
    func getFacilities() -> [String]{
        let facilities  = ["Geyser","Lift/Elevator","Card Payment","Free WIfI","Parking Facility","Public Space CCTV",
                           "Laundry","Dining Area","InHouse Restaurant","Conference Room", "Banquet Hall", "Mini-Fridge",
                           "In-Room Safe","Power Back-up" , "Hair Dryer", "Wheelchair Accessible","CCTV Surveillance Cameras","Swimming Pool","Pet Friendly", "Bar"]
        return facilities
    }
}
