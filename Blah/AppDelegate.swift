//
//  AppDelegate.swift
//  Blah
//
//  Created by Jlabs on 9/30/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
            //configuring the tab style of hotels tab
        let tabVC = self.window?.rootViewController as! UITabBarController
        let tabBar = tabVC.tabBar
        tabBar.tintColor = UIColor(colorLiteralRed: 219/255, green: 90/255, blue: 110/255, alpha: 1)
        let hotelTab = tabBar.items?[1]
        let tabImageFilled = #imageLiteral(resourceName: "hotel-tab-icon-selected").withRenderingMode(.alwaysOriginal)
        let tabImageUnFilled = #imageLiteral(resourceName: "hotel-tab-icon-unselected").withRenderingMode(.alwaysOriginal)
        hotelTab?.selectedImage = tabImageFilled
        hotelTab?.image = tabImageUnFilled
        
        //configuring google maps api
        GMSServices.provideAPIKey("AIzaSyA5vm7b2wxyS1hJGpHJMDGLwj4Pa6Sodgo")
        
        //configuring access token for the device
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        print("requesting \(APIEndPoints().getAccessToken(deviceID))")
        Alamofire.request(APIEndPoints().getAccessToken(deviceID)).responseJSON{
            response in
            switch (response.result){
            case .failure :
                let dialog = HelpingCode().createDialogBox(fromMessage: "Unable to connect to server, please check your network", havingTitle: "Oops!", withAction: "Ok")
                self.window?.rootViewController?.present(dialog, animated: true, completion: nil)
            case .success:
                let json = response.result.value as! [String : Any]
                print(json)
                if let success = json["success"] as? Bool{
                    switch success{
                    case false:
                        let dialog = HelpingCode().createDialogBox(fromMessage: "Encountered an error, please relaunch after sometim", havingTitle: "Oops!", withAction: "Got it!")
                        self.window?.rootViewController?.present(dialog, animated: true, completion: nil)
                    case true:
                        DataModal.sharedInstance.token = json["access_token"] as! String
                        print("your token \(DataModal.sharedInstance.token)")
                    }
                }
                
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

