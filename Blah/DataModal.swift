//
//  DataModal.swift
//  Blah
//
//  Created by Jatin Garg on 07/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import Foundation
import UIKit
class DataModal {
    var selectedCity: String!
    var checkInDate: Date? = nil
    var checkOutDate: Date? = nil
    var cityCode: String!
    var token: String!
    class var sharedInstance: DataModal{
        struct Static{
            static let instance: DataModal = DataModal()
        }
        return Static.instance
    }
}
