//
//  PriceFilterTableViewCell.swift
//  Blah
//
//  Created by Jatin Garg on 20/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit

class PriceFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var priceText: UILabel!
    @IBOutlet weak var slider: RangeSlider!
    var helper = HelpingCode()
    var priceSlider: RangeSlider!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        slider.minimumValue = 800
        slider.maximumValue = 12000
        slider.upperValue = 10000
        slider.lowerValue = 1000
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        slider.trackHighLightTintColor = helper.returnAccentColor()
       
        
    }
    @IBAction func sliderChanged(_ sender: AnyObject) {
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func sliderValueChanged(slider: RangeSlider){
        priceText.text = "Rs. \(Int(slider.lowerValue)) - \(Int(slider.upperValue))"
    }
    
}
