//
//  FacilitiesCellSubView.swift
//  Blah
//
//  Created by Jatin Garg on 26/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit

class FacilitiesCellSubView: UIView {

    @IBOutlet weak var facilityLabel: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    
    var isChecked = false
    var facility: String!{
        didSet{
            facilityLabel?.text = facility
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        backgroundColor = UIColor.blue
        let nib = Bundle.main.loadNibNamed("FacilitiesCellSubView", owner: self, options: nil)?.first as! UIView
        nib.frame = bounds
        nib.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(nib)
        nib.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTapped)))
    }
    
    @IBAction func checkBtnTapped(_ sender: AnyObject) {
        toggleStates()
    }
    func viewTapped(gesture: UITapGestureRecognizer){
        toggleStates()
        
    }
    
    func toggleStates(){
        if !isChecked{
            checkBtn.setImage(#imageLiteral(resourceName: "checkbox-checked"), for: .normal)
            isChecked = true
        }
        else{
            checkBtn.setImage(#imageLiteral(resourceName: "checkbox-unchecked"), for: .normal)
            isChecked = false
        }
    }
}
