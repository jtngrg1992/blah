//
//  HotelEntity.swift
//  Blah
//
//  Created by Jlabs on 10/12/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import Foundation

class HotelEntity{
    var hotelName: String? = nil
    var hotelAddress: String? = nil
    var hotelImages = [String]()
    var hotelDisplayPrice: Int? = nil
    var hotelDiscount: Int? = nil
    
    init(name: String, images: [String],address: String){
        self.hotelName = name
        self.hotelImages = images
        self.hotelAddress = address
    }
}
