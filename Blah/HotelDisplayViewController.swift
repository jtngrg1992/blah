//
//  HotelDisplayViewController.swift
//  Blah
//
//  Created by Jatin Garg on 15/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import GoogleMaps

class HotelDisplayViewController: UIViewController {

    @IBOutlet weak var imageScroll: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var bookBtn: UIButton!
    
    var hotelToDisplay: HotelEntity? = nil
    var tableViewFrameOrigin: CGFloat!
    var helper = HelpingCode()
    var beginningOffset: CGFloat!
    var dontResizeImagesFlag: Bool = false
    var lastContentOffset: CGFloat!
    var tableAlpha: Float! = 0
    var tableOriginalAlpha: Float!
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.backgroundColor = UIColor.clear
        tableView.backgroundColor = UIColor.clear
        prepareimageScroll()
        headerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(headerTapped)))
        headerView.frame.size.height = 300
        closeBtn.layer.masksToBounds = true
        closeBtn.layer.cornerRadius = 3
        closeBtn.isHidden = true
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: (self.navigationController?.navigationBar.frame.height)! / 2))
        titleLabel.attributedText = helper.returnSpacedOutString(text: (hotelToDisplay?.hotelName)!)
        titleLabel.font = UIFont(name: "Avenir Next", size: 14)
        self.navigationItem.titleView = titleLabel
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeLeft.direction = .left
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
        swipeRight.direction = .right
        headerView.addGestureRecognizer(swipeLeft)
        headerView.addGestureRecognizer(swipeRight)
        tableAlpha =  Float(tableView.alpha)
        tableOriginalAlpha = Float(tableView.alpha)
        }
   
    func headerTapped(gestureRecognizer: UITapGestureRecognizer){
        UIView.animate(withDuration: 0.6) {
            self.closeBtn.isHidden = false
            self.tableView.frame.origin.y += self.view.frame.height
            self.bookBtn.frame.origin.y += 2 * self.bookBtn.frame.height
        }
        resetImageview()
    }
    
    func handleSwipe(gestureRecognizer: UISwipeGestureRecognizer){
        switch gestureRecognizer.direction.rawValue {
        case 1:
            print("swped right")
            var img: UIImageView? = nil
            let origin = imageScroll.contentOffset.x - self.view.frame.width
            imageScroll.subviews.forEach({ (imageView) in
                if let temp = imageView as? UIImageView{
                    if temp.frame.origin.x == origin{
                        img = temp
                        imageScroll.scrollRectToVisible((img?.frame)!, animated: true)
                    }
                }
            })
            
        case 2:
            var img: UIImageView? = nil
            let origin = imageScroll.contentOffset.x + self.view.frame.width
            imageScroll.subviews.forEach({ (imageView) in
                if let temp = imageView as? UIImageView{
                    if temp.frame.origin.x == origin{
                        img = temp
                        imageScroll.scrollRectToVisible((img?.frame)!, animated: true)
                    }
                }
            })
            

        default: return
        }
    }
    
    func resetImageview(){
        self.imageScroll.subviews.forEach({ (view) in
            guard let imageView = view as? UIImageView
                else{
                    return
            }
            
            imageView.frame.size.height = self.view.frame.height
        })

    }
    @IBAction func closePressed(_ sender: AnyObject) {
        UIView.animate(withDuration: 0.6) {
            self.closeBtn.isHidden = true
            self.tableView.frame.origin.y = 0
            self.bookBtn.frame.origin.y -= 2 * self.bookBtn.frame.height
        }
        dontResizeImagesFlag = true
        tableView.contentOffset.y = 0
    }
    func prepareimageScroll(){
        imageScroll.frame = view.frame
        imageScroll.isPagingEnabled = true
        imageScroll.backgroundColor = UIColor.black
        for i in 0...(hotelToDisplay?.hotelImages.count)! - 1{
            let xpos = self.view.frame.width * CGFloat(i)
            let imageView = UIImageView(frame: CGRect(x: xpos, y: 0, width: imageScroll.frame.width, height: imageScroll.frame.height))
            imageView.sd_setImage(with: URL(string: (hotelToDisplay?.hotelImages[i])!))
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageScroll.addSubview(imageView)
            imageScroll.contentSize.width = imageScroll.frame.width * CGFloat(i+1)
            
        }
    }
    
     func loadMap() -> GMSMapView{
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: 33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: view.frame.width, height: 200), camera: camera)
        mapView.isMyLocationEnabled = true
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
        mapView.moveCamera(.setCamera(camera))
        return mapView
    }
}

extension HotelDisplayViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell") as! TitleCell
            cell.titleLabel.text = hotelToDisplay?.hotelName
            cell.addressLabel.text = hotelToDisplay?.hotelAddress
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell")
            return cell!
    
        default:
            return UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if(-scrollView.contentOffset.y > 50 ){
            UIView.animate(withDuration: 1, animations: {
                self.closeBtn.isHidden = false
                self.tableView.frame.origin.y += self.view.frame.height
                self.bookBtn.frame.origin.y += 2 * self.bookBtn.frame.height
            })
        }
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.beginningOffset = scrollView.contentOffset.y
        lastContentOffset = scrollView.contentOffset.y
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if dontResizeImagesFlag {
            dontResizeImagesFlag = false
            return
        }
        let finalOffset = scrollView.contentOffset.y - beginningOffset
        imageScroll.subviews.forEach { (view) in
            guard let imageView = view as? UIImageView
                else{
                    return
            }
            imageView.frame.size.height -= finalOffset
        }
        beginningOffset = scrollView.contentOffset.y
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return 400
        }
        return 70
    }
    
    
    
}
