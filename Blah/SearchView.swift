//
//  SearchView.swift
//  Blah
//
//  Created by Jlabs on 10/6/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import Foundation
import UIKit

protocol SearchViewDelegate{
    func userDidSearch(searchString: String)
    func searchViewTouched(view: UITextField)
}
class SearchView: UIView{
    @IBOutlet weak var searchField: UITextField!
    
    var delegate: SearchViewDelegate!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    
    func setUp(){
        let nib = Bundle.main.loadNibNamed("SearchView", owner: self, options: nil)?.first as! UIView
        nib.frame = self.frame
        nib.backgroundColor = UIColor.clear
        searchField.backgroundColor = UIColor(colorLiteralRed: 219/255, green: 90/255, blue: 110/255, alpha: 1)
        searchField.leftViewMode = .unlessEditing
        searchField.leftView = UIImageView(image: #imageLiteral(resourceName: "search"))
        searchField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)
        searchField.addTarget(self, action: #selector(textFieldWillInit), for: .editingDidBegin)
        self.addSubview(nib)
    }
    
    
    
    func textFieldChanged(_ textField: UITextField) {
        if delegate != nil{
            delegate.userDidSearch(searchString: textField.text!)
        }
    }
    
    func textFieldWillInit(textField: UITextField){
        if delegate != nil{
            delegate.searchViewTouched(view: textField)
        }
    }
}
