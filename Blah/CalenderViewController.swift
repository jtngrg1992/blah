//
//  CalenderViewController.swift
//  Blah
//
//  Created by Jlabs on 10/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import PDTSimpleCalendar

protocol CalendarViewDelegate {
    func didFinishChoosingDates()
    
}
class CalenderViewController: UIViewController, PDTSimpleCalendarViewDelegate {
    
    @IBOutlet var indicatorLine: UIView!
    @IBOutlet weak var checkInStack: UIStackView!
    @IBOutlet weak var checkOutStack: UIStackView!
    @IBOutlet weak var navigationBarTitle: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    var helpingCode: HelpingCode!
    var calenderInstance: PDTSimpleCalendarViewController!
    var indicator: UIView!
    var checkDifference: Int!
    var indicatorPosition = "checkIn"
    var delegate: CalendarViewDelegate? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helpingCode = HelpingCode()
        configureHeader()
        configureCalenderView()
        let checkinTap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        checkInStack.addGestureRecognizer(checkinTap)
        let checkOutTap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        checkOutStack.addGestureRecognizer(checkOutTap)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        addIndicatorLine()
    }
    
    func simpleCalendarViewController(_ controller: PDTSimpleCalendarViewController!, didSelect date: Date!) {
       print(date)
        let stringDate = helpingCode.getStringDate(from: date, format: "dd-MMM-yyyy")
        if indicatorPosition == "checkOut"{
            (checkOutStack.viewWithTag(1) as? UILabel)?.text = stringDate
            DataModal.sharedInstance.checkOutDate = date
            moveIndicator(to: checkInStack)
            indicatorPosition = "checkIn"
            
        }
        else{
            DataModal.sharedInstance.checkInDate = date
            (checkInStack.viewWithTag(1) as? UILabel)?.text = stringDate
            moveIndicator(to: checkOutStack)
            (checkOutStack.viewWithTag(1) as? UILabel)?.text = ""
            indicatorPosition = "checkOut"
            
        }
    
    }
   
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        guard let inDate = DataModal.sharedInstance.checkInDate,
        let outDate = DataModal.sharedInstance.checkOutDate
            else{
                let alert = helpingCode.createDialogBox(fromMessage: "Please select valid dates!", havingTitle: "Derp!", withAction: "Got it!")
                self.present(alert, animated: true, completion: nil)
                return
        }
        if helpingCode.isSmaller(date:inDate, from: outDate) == "smaller"{
            dismiss(animated: true, completion: { 
                self.delegate?.didFinishChoosingDates()
            })
        }
        
        else{
            let alert = helpingCode.createDialogBox(fromMessage: "Check-in date should come before the check-out date", havingTitle: "Derp!", withAction: "Got it!")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func handleTap(recognizer: UITapGestureRecognizer){
        
        UIView.animate(withDuration: 0.4) { 
            self.indicator.frame.origin.x = (recognizer.view?.frame.origin.x)!
        }
    }
    

    func configureHeader(){
        navigationBarTitle.attributedText = HelpingCode().returnSpacedOutString(text: "Pick Dates")
        headerView.backgroundColor = helpingCode.returnAccentColor()
        
    }
    
    func configureCalenderView(){
        calenderInstance = PDTSimpleCalendarViewController()
        calenderInstance.delegate = self
        calenderInstance.weekdayHeaderEnabled = true
        calenderInstance.weekdayTextType = .short
        calenderInstance.firstDate = NSDate() as Date
        self.addChildViewController(calenderInstance)
        
        let calenderFrame = CGRect(x: 0, y: 150, width: view.frame.width, height: view.frame.height)
        let c = calenderInstance.view
        c?.frame = calenderFrame
        self.view.addSubview(c!)

    }
    
    func addIndicatorLine(){
        indicator = UIView(frame: CGRect(x: checkInStack.frame.origin.x, y: 70 + checkInStack.frame.height, width: checkInStack.frame.width, height: 2))
        indicator.backgroundColor = helpingCode.returnAccentColor()
        self.view.addSubview(indicator)
    }
    
    func moveIndicator(to newView: UIView!){
        UIView.animate(withDuration: 0.4) {
            self.indicator.frame.origin.x = newView.frame.origin.x
        }
    }
}
