//
//  CitiCollectionViewCell.swift
//  Blah
//
//  Created by Jlabs on 9/30/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit

class CitiCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cityImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    
    
}
