//
//  RoomFilterTableViewCell.swift
//  Blah
//
//  Created by Jatin Garg on 24/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit

class RoomFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var guestView: UIView!
    @IBOutlet weak var guestLabel: UILabel!
    @IBOutlet weak var guestSubtract: UIButton!
    @IBOutlet weak var guestAdd: UIButton!
    
    @IBOutlet weak var roomView: UIView!
    @IBOutlet weak var roomLabel: UILabel!
    @IBOutlet weak var roomSubtract: UIButton!
    @IBOutlet weak var roomAdd: UIButton!
    
    var numberOfGuests = 1{
        didSet{
            let labelText = numberOfGuests > 1 ? "Guests" : "Guest"
            guestLabel.text = "\(numberOfGuests) \(labelText)"
        }
    }
    var numberOfRooms = 1{
        didSet{
            let labelText = numberOfRooms > 1 ? "Rooms" : "Room"
            roomLabel.text = "\(numberOfRooms) \(labelText)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        guestView.subviews.forEach { (v) in
            if let btn = v as? UIButton{
                btn.layer.cornerRadius = btn.frame.width/2
                btn.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 15, 10)
            }
        }
        roomView.subviews.forEach { (v) in
            if let btn = v as? UIButton{
                btn.layer.cornerRadius = btn.frame.width/2
                btn.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 15, 10)
            }
        }
    }

    
    @IBAction func guestAdded(_ sender: AnyObject) {
        numberOfGuests += 1
        
    }
    @IBAction func guestsRemoved(_ sender: AnyObject) {
        if numberOfGuests > 1{
            numberOfGuests -= 1
        }
    }
    
    @IBAction func roomAdded(_ sender: AnyObject) {
        numberOfRooms += 1
    }
    
    @IBAction func roomRemoved(_ sender: AnyObject) {
        if numberOfRooms > 1{
            numberOfRooms -= 1
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
