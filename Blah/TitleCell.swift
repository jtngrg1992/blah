//
//  TitleCell.swift
//  Blah
//
//  Created by Jatin Garg on 15/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import Cosmos

class TitleCell: UITableViewCell {

    @IBOutlet weak var secondaryView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.secondaryView.backgroundColor = UIColor.white
        secondaryView.layer.cornerRadius = 3
        }
    @IBAction func showMapVC(_ sender: UIButton) {
        print("Hello\(titleLabel.text)")
    }
}
