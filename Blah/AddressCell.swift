//
//  AddressCell.swift
//  Blah
//
//  Created by Jatin Garg on 15/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var secondaryView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
