//
//  HotelsViewHeaderView.swift
//  Blah
//
//  Created by Jlabs on 10/7/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit


protocol headerViewDelegate{
    func didTapCalenderBtn()
}
class HotelsViewHeaderView: UIView {

    @IBOutlet weak var calenderBtn: UIButton!
    @IBOutlet weak var checkInDate: UILabel!
    @IBOutlet weak var checkOutDate: UILabel!
    @IBOutlet weak var roomsBooked: UILabel!
    @IBOutlet weak var peopleAriving: UILabel!
    
    var delegate: headerViewDelegate? = nil
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        let loadedNib = Bundle.main.loadNibNamed("HotelsViewHeaderView", owner: self, options: nil)?.first as! UIView
        loadedNib.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        loadedNib.backgroundColor = UIColor(colorLiteralRed: 219/255, green: 90/255, blue: 110/255, alpha: 1)
    
        for v in loadedNib.subviews{
            if let btn = v as? UIButton{
                btn.imageView?.contentMode = .scaleAspectFit
            }
        }
//        calenderBtn.imageView?.contentMode = .scaleAspectFit
        self.addSubview(loadedNib)
    }

    @IBAction func didPressCalenderBtn(_ sender: UIButton) {
        if delegate != nil{
            delegate?.didTapCalenderBtn()
        }
    }
}
