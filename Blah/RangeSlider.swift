//
//  RangeSlider.swift
//  Blah
//
//  Created by Jatin Garg on 21/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import QuartzCore



@IBDesignable class RangeSlider: UIControl {
    @IBInspectable  var minimumValue:Double = 0.0{
        didSet{
            updateLayerFrames()
        }
    }
    @IBInspectable var maximumValue:Double = 1.0{
        didSet{
            updateLayerFrames()
        }
    }

    @IBInspectable var lowerValue:Double = 0.2{
        didSet{
            updateLayerFrames()
        }
    }

    @IBInspectable var upperValue:Double = 0.8{
        didSet{
            updateLayerFrames()
        }
    }

    var previousLocation = CGPoint()
    @IBInspectable var trackTintColor: UIColor = UIColor(white: 0.9, alpha: 1.0){
        didSet{
            trackLayer.setNeedsDisplay()
        }
    }
    @IBInspectable var trackHighLightTintColor: UIColor = UIColor(red: 0.0, green: 0.45, blue: 0.94, alpha: 1.0){
        didSet{
            trackLayer.setNeedsDisplay()
        }
    }
    @IBInspectable var thumbTintColor: UIColor = UIColor.white{
        didSet{
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
        }
    }
    @IBInspectable var curvaceousness: CGFloat = 1.0{
        didSet{
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
            trackLayer.setNeedsDisplay()
        }
    }

    let lowerThumbLayer = RangeSliderThumbLayer()
    let upperThumbLayer = RangeSliderThumbLayer()
    let trackLayer = RangeSliderTrackLayer()
    
    var thumbWidth: CGFloat{
        return CGFloat(bounds.height)
    }
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        trackLayer.contentsScale = UIScreen.main.scale
        lowerThumbLayer.contentsScale = UIScreen.main.scale
        upperThumbLayer.contentsScale = UIScreen.main.scale
        
        layer.addSublayer(trackLayer)
        layer.addSublayer(lowerThumbLayer)
        layer.addSublayer(upperThumbLayer)
        
        
        lowerThumbLayer.rangeSlider = self
        upperThumbLayer.rangeSlider = self
        trackLayer.rangeSlider = self
        updateLayerFrames()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        trackLayer.contentsScale = UIScreen.main.scale
        lowerThumbLayer.contentsScale = UIScreen.main.scale
        upperThumbLayer.contentsScale = UIScreen.main.scale
        
        layer.addSublayer(trackLayer)
        layer.addSublayer(lowerThumbLayer)
        layer.addSublayer(upperThumbLayer)
        
        
        lowerThumbLayer.rangeSlider = self
        upperThumbLayer.rangeSlider = self
        trackLayer.rangeSlider = self
    }
    
    override func layoutSubviews() {
        updateLayerFrames()
    }
    func updateLayerFrames(){
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        trackLayer.frame = bounds.insetBy(dx: 0.0, dy: bounds.height/3)
        trackLayer.setNeedsDisplay()
        
        let lowerThumbCenter = CGFloat(positionForValue(value: lowerValue))
        lowerThumbLayer.frame = CGRect(x: lowerThumbCenter - thumbWidth / 2.0, y: 0.0,
                                       width: thumbWidth, height: thumbWidth)
        print(lowerThumbLayer.frame)
        lowerThumbLayer.setNeedsDisplay()
        
        let upperThumbCenter = CGFloat(positionForValue(value: upperValue))
        upperThumbLayer.frame = CGRect(x: upperThumbCenter - thumbWidth / 2.0, y: 0.0,
                                       width: thumbWidth, height: thumbWidth)
        upperThumbLayer.setNeedsDisplay()
        CATransaction.commit()
    }
    
    func positionForValue(value: Double) -> Double {
        return Double(bounds.width - thumbWidth) * (value - minimumValue) /
            (maximumValue - minimumValue) + Double(thumbWidth / 2.0)
    }
    
    func boundValue(value: Double, toLowerValue lowerValue: Double, upperValue: Double) -> Double {
        return min(max(value, lowerValue), upperValue)
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        previousLocation = touch.location(in: self)
        if lowerThumbLayer.frame.contains(previousLocation){
            lowerThumbLayer.isHighlighted = true
        }
        else if upperThumbLayer.frame.contains(previousLocation){
            upperThumbLayer.isHighlighted = true
        }
        
        
        return lowerThumbLayer.isHighlighted || upperThumbLayer.isHighlighted
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        if lowerThumbLayer.frame.intersects(upperThumbLayer.frame){
            lowerThumbLayer.frame.origin.x -= 10
            return true
        }
        let location = touch.location(in: self)
        // 1. Determine by how much the user has dragged
        let deltaLocation = Double(location.x - previousLocation.x)
        let deltaValue = (maximumValue - minimumValue) * deltaLocation / Double(bounds.width - thumbWidth)
        
        previousLocation = location
        
        // 2. Update the values
        if lowerThumbLayer.isHighlighted {
            lowerValue += deltaValue
            lowerValue = boundValue(value: lowerValue, toLowerValue: minimumValue, upperValue: upperValue)
        } else if upperThumbLayer.isHighlighted {
            upperValue += deltaValue
            upperValue = boundValue(value: upperValue, toLowerValue: lowerValue, upperValue: maximumValue)
        }
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        updateLayerFrames()
        CATransaction.commit()
        sendActions(for: .valueChanged)
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        lowerThumbLayer.isHighlighted = false
        upperThumbLayer.isHighlighted = false
        
    }
    
   }
