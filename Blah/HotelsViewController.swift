//
//  HotelsViewController.swift
//  Blah
//
//  Created by Jlabs on 10/6/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import PDTSimpleCalendar
import Alamofire


class HotelsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, headerViewDelegate, CalendarViewDelegate {
    
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinnder: UIActivityIndicatorView!
    
    var city: String? = ""
    var hotels = [HotelEntity]()
    var searchView:  SearchView!
    var headerHeight: CGFloat = 40
    var headerView: HotelsViewHeaderView!
    var helpingCode = HelpingCode()
    let cities = CityCodes().codes
    var selectedHotel: HotelEntity? = nil
    var selectedCity: String? = nil{
        didSet{
            spinnder?.startAnimating()
            fetchHotelsForCurrentCity()
            hotels = [HotelEntity]()
            tableView?.reloadData()
        }
    }
    var cityCode: String!
    var lastContentOffset: Float = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView = HotelsViewHeaderView(frame: CGRect(x: 3, y: 5, width: self.view.bounds.width - 6, height: headerHeight))
        headerView.peopleAriving.text = "40"
        headerView.roomsBooked.text = "100"
        headerView.backgroundColor = helpingCode.returnAccentColor()
        headerView.layer.cornerRadius = 3
        headerView.layer.masksToBounds = true
        headerView.alpha = 0.85
        headerView.delegate = self
        self.view.addSubview(headerView)
        spinnder.hidesWhenStopped = true
        spinnder.startAnimating()
    }
    
    @IBAction func filterBtnPressed(_ sender: UIButton) {
        let filterVC = FiltersViewController()
        present(filterVC, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let searchView = SearchView(frame: CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.frame.width)!/2, height: (self.navigationController?.navigationBar.frame.height)!/2))
        if let city = selectedCity{
            searchView.searchField.text = "Hotels in \(city)"
        }
        
        self.navigationItem.titleView = searchView
        city = DataModal.sharedInstance.selectedCity
        
        let currentDate = Date()
        let calender = NSCalendar(calendarIdentifier: .gregorian)
        var offset = DateComponents()
        offset.day = 1
        let nextDate = calender?.date(byAdding: offset, to: currentDate, options: .init(rawValue: 0))
        headerView.checkInDate.text = helpingCode.getStringDate(from: currentDate, format: "MMM d")!
        headerView.checkOutDate.text = helpingCode.getStringDate(from: nextDate!, format: "MMM d")!
        populateCatalogHeader()
        basicConfigration()
        //headerView = HotelsViewHeaderView(frame: CGRect(x: 3, y: 5, width: self.view.bounds.width - 6, height: headerHeight))
    }
  

    func basicConfigration(){
        self.navigationController?.navigationBar.barTintColor = helpingCode.returnAccentColor()
        filterBtn.backgroundColor = helpingCode.returnAccentColor()
        filterBtn.layer.cornerRadius = filterBtn.frame.width/2
        filterBtn.layer.shadowColor = UIColor.black.cgColor
        filterBtn.layer.shadowOffset = CGSize(width: 3 , height: 3)
        filterBtn.layer.shadowRadius = 2
        filterBtn.layer.shadowOpacity = 0.8
        
    }
    
    func didTapCalenderBtn() {
        let calenderVC = CalenderViewController()
        calenderVC.delegate = self
        present(calenderVC, animated: true) { 
            //
        }
    }
    
    func populateCatalogHeader(){
        guard let inDate = DataModal.sharedInstance.checkInDate,
            let outDate = DataModal.sharedInstance.checkOutDate
            else{
                return
        }
        let checkIn = helpingCode.getStringDate(from: inDate, format: "MMM d")
        let checkOut = helpingCode.getStringDate(from: outDate, format: "MMM d")
        headerView.checkInDate.text = checkIn!
        headerView.checkOutDate.text = checkOut!

    }
    
    func fetchHotelsForCurrentCity(){
        hotels = [HotelEntity]()
        print(APIEndPoints().fetchHotelsByCity(self.cityCode))
        Alamofire.request(APIEndPoints().fetchHotelsByCity(cityCode)).validate(statusCode: 200..<300)
            .responseJSON { response in
             switch(response.result){
               case .success :
               let json = response.result.value as! [String : Any]
               self.parseData(data: json)
               default: print("Request Failure")
            }
        }
    }
    
    func parseData(data: [String: Any]){
        let j = data["data"] as! NSArray
        j.forEach { (item) in
            let item = item as! [String : Any]
            hotels.append(HotelEntity(name: item["hotelname"] as! String, images: item["pictures"] as! [String],address: item["location"] as! String))
        }
        print("Done getting")
        tableView.reloadData()
        if self.spinnder.isAnimating{
            spinnder.stopAnimating()
        }
    }
    
    //preparing destination VC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destinationVC = segue.destination as? HotelDisplayViewController
            else{
                return
        }
        destinationVC.hotelToDisplay = selectedHotel
        
    }

    
}

//delegate methods

extension HotelsViewController{
    
    //tableview delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count
    }
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HotelCatalogCell
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        let display = hotels[indexPath.row]
        cell.hotelTitle.text = display.hotelName
        print(display.hotelImages[0])
        
        cell.displayImage.sd_setImage(with: URL(string: display.hotelImages[0])!)
        return cell
    }
    
    @objc(tableView:didSelectRowAtIndexPath:) func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedHotel = hotels[indexPath.row]
        performSegue(withIdentifier: "segueToHotel", sender: nil)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        lastContentOffset = Float(scrollView.contentOffset.y)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if Float(scrollView.contentOffset.y) < lastContentOffset{
            //down
        }
        else{
            //up
        }
        lastContentOffset  = Float(scrollView.contentOffset.y)
    }
    
    //Calender Delegate Function
    func didFinishChoosingDates(){
        //checking if dates are nil in data modal
        populateCatalogHeader()
    }
}
