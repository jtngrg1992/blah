//
//  HelpingCode.swift
//  Blah
//
//  Created by Jlabs on 10/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import Foundation
import Alamofire

struct HelpingCode{
    
    public var months: [Int : String] = [1 : "January", 2 : "February" , 3 : "March" , 4 : "April" , 5  : "May" , 6 : "June",
                                         7 : "July" , 8 : "August" , 9 : "September" ,10 : "October" , 11 : "November" , 12 : "December"]
    func returnSpacedOutString(text: String) ->NSMutableAttributedString{
        let aS = NSMutableAttributedString(string: text)
        aS.addAttribute(NSKernAttributeName, value: 5.0, range: NSMakeRange(0, text.characters.count))
        return aS
    }
    
    func returnAccentColor() -> UIColor{
        return UIColor(colorLiteralRed: 219/255, green: 90/255, blue: 110/255, alpha: 1)
    }
    
    func returnHeaderColor() -> UIColor{
        return UIColor(colorLiteralRed: 241, green: 240, blue: 246, alpha: 1)
    }
    
    func getStringDate(from date: Date, format: String) -> String?{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    func getDaysDifference(from startDate: Date, to endDate: Date, format: String) -> Int{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let cal = Calendar.current
        let components = cal.dateComponents([.day], from: startDate, to: endDate)
        let dayDifference = components.day
        return dayDifference!
    }
    
    func isSmaller(date: Date, from endDate: Date) -> String{
        switch(date.compare(endDate)){
        case .orderedAscending :
            return("smaller")
        case .orderedDescending :
            return("bigger")
        default:
            return("equal dates")
        }
        
    }
    
    func getDate(from: String, format: String) -> Date?{
        let formatter = DateFormatter()
        return formatter.date(from: from)
    }
    
    func createDialogBox(fromMessage message: String, havingTitle title: String, withAction action: String) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: action, style: .default, handler: nil))
        return alert
    }
    
}
