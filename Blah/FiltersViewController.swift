//
//  FiltersViewController.swift
//  
//
//  Created by Jlabs on 10/13/16.
//
//

import UIKit

struct SectionLabels{
    var title: String!
    var imageName: String!
}
class FiltersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var sideStack: UIStackView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footerTitle: UIView!
    
    var helper = HelpingCode()
    var insetApplied = false
    var lastSectionAccessed: Int!
    var visibleSection: Int! = 0
    var sectionLabels = [SectionLabels]()
    var facilitiesToggled = false
    var counter = 0
    var toggleText = "+More"
    var toggleBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.backgroundColor = helper.returnAccentColor()
        headerTitle.attributedText = helper.returnSpacedOutString(text: "Filters")
        footerTitle.backgroundColor = helper.returnAccentColor()
        sectionLabels = [SectionLabels(title: "Guest & Rooms", imageName: "bed"),SectionLabels(title: "Price Range", imageName: "price"),SectionLabels(title: "Facilities", imageName: "facilities"),SectionLabels(title: "Collections", imageName: "collections")]
        tableView.register(UINib(nibName: "PriceFilterTableViewCell", bundle: nil), forCellReuseIdentifier: "priceFilterCell")
        tableView.register(UINib(nibName: "RoomFilterTableViewCell", bundle: nil), forCellReuseIdentifier: "roomFilterCell")
    }
    
    @IBAction func closeBtnPressed(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func categoryPressed(_ sender: UIButton) {
        var section = 0
        unHighlightButtons()
        sender.isSelected = true
        for i in 0...sideStack.subviews.count - 1{
            if let btn = sideStack.subviews[i] as? UIButton{
                if !btn.isSelected{
                    section += 1
                }
                else{
                    break
                }
            }
        }
        
        
        let i = IndexPath(row: 0, section: section)
        tableView.scrollToRow(at: i , at: .top, animated: true)
        lastSectionAccessed = i.section
    }
    
    func adjustBottomInset(){
        if tableView.contentOffset.y >= (tableView.contentSize.height - tableView.frame.size.height){
            print("scroll to bottom")
            let visibleIndexes = tableView.indexPathsForVisibleRows
            if visibleIndexes?.count == 0{
                return
            }
            var visibleSections = [Int]()
            for i in 0...(visibleIndexes?.count)! - 1{
                if visibleSections.contains((visibleIndexes?[i].section)!){
                    continue
                }
                visibleSections.append((visibleIndexes?[i].section)!)
            }
            
            print("Visible sections \(visibleSections)")
            var totalInset: CGFloat = 0
            for i in 0..<visibleSections.count - 1{
                let rect = tableView.rect(forSection: visibleSections[i])
                totalInset += rect.height
            }
            print("total inset required \(totalInset)")
            if(!insetApplied){
                tableView.contentInset.bottom = totalInset
                print("INset applied")
                insetApplied = true
                if lastSectionAccessed == nil{
                    return
                }
                let i = IndexPath(row: 0, section: lastSectionAccessed)
                tableView.scrollToRow(at: i, at: .top, animated: true)
            }
        }
        
        
    }
    
    func unHighlightButtons(){
        sideStack.subviews.forEach { (v) in
            if let btn = v as? UIButton{
                btn.isSelected = false
            }
        }
    }
    
}

extension FiltersViewController{
    
    @objc(numberOfSectionsInTableView:) func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0 : let cell = tableView.dequeueReusableCell(withIdentifier: "roomFilterCell")
            return cell!
        case 1 :
            let cell = tableView.dequeueReusableCell(withIdentifier: "priceFilterCell")
            
            return cell!
        case 2 :
            var cell = tableView.dequeueReusableCell(withIdentifier: "facilityCell")
            if cell == nil{
                cell = UITableViewCell(style: .default, reuseIdentifier: "facilityCell")
            }
            
            let facilities = getFacilities()
            counter = 0
            cell?.contentView.subviews.forEach({ (v) in
                v.removeFromSuperview()
            })
            facilities.forEach({ (facility) in
                if !facilitiesToggled && counter > 5{
                    return
                }
                let ypos = counter * 50
                let viewWidth = Int((cell?.contentView.frame.width)!)
                let v = FacilitiesCellSubView(frame: CGRect(x: 0, y: ypos, width: viewWidth, height: 50))
                v.facility = facility
                cell?.contentView.addSubview(v)
                counter += 1
                
            })
            
            toggleBtn = UIButton(frame: CGRect(x: Int((cell?.contentView.bounds.origin.x)! + 15), y: counter * 50, width: 60, height: 30))
            toggleBtn.setTitle("\(toggleText)", for: .normal)
            toggleBtn.setTitleColor(helper.returnAccentColor(), for: .normal)
            toggleBtn.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 11.0)
            toggleBtn.addTarget(self, action: #selector(toggleBtnPressed), for: .touchDown)
            cell?.contentView.addSubview(toggleBtn)
            return cell!
            
        default:
            var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            if cell == nil{
                cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            }
            cell?.textLabel?.text = "\(indexPath.row)"
            
            return cell!

            
        }
    }
    
    
    func rangeSelectorValueChanged(rangeSlider: RangeSlider){
        print("Range slider value changed , upper value  \(rangeSlider.upperValue), lower value \(rangeSlider.lowerValue)")
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func toggleBtnPressed(btn: UIButton){
        if !facilitiesToggled{
            toggleText = "-Less"
            facilitiesToggled = true
        }
        else{
            facilitiesToggled = false
            toggleText = "+More"
        }
        
        tableView.reloadData()
    }
    @objc(tableView:heightForRowAtIndexPath:) func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2{
            return CGFloat(counter + 1) * 50
        }
        return 100
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let v = FilterViewSectionHeader(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        v.titleLabel.text = sectionLabels[section].title
        v.titleImage.image = UIImage(named: sectionLabels[section].imageName)
       return v
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(tableView.indexPathsForVisibleRows?.count == 0){
            return
        }
        let firstVisibleCell = tableView.indexPathsForVisibleRows?[0]
        if visibleSection != firstVisibleCell?.section{
            unHighlightButtons()
            visibleSection = firstVisibleCell?.section
            let btn = sideStack.viewWithTag(visibleSection + 1) as? UIButton
            btn?.isSelected = true
            
        }
        adjustBottomInset()
            }
    
    //slider delegate functions
    
    
}

