//
//  FilterViewSectionHeader.swift
//  Blah
//
//  Created by Jatin Garg on 14/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit

class FilterViewSectionHeader: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    
    var helper = HelpingCode()
    var imageSize = CGSize(width: 40, height: 40)

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    func setUp(){
        let nib = Bundle.main.loadNibNamed("FilterViewSectionHeader", owner: self, options: nil)?.first as! UIView
        nib.frame = self.frame
        self.addSubview(nib)
        
    }
}
