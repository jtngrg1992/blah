//
//  ViewController.swift
//  Blah
//
//  Created by Jlabs on 9/30/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit
import Alamofire

class InitialViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{

    @IBOutlet weak var myCollectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var myCollectionView: UICollectionView!
    private let leftAndRightPaddings: CGFloat = 5
    private let numberOfItemsPerRow: CGFloat = 1
    private let cities = CityCodes().codes
    override func viewDidLoad() {
        super.viewDidLoad()
        let widthOfCell = view.frame.width - (2 * leftAndRightPaddings)
        myCollectionViewLayout.sectionInset = UIEdgeInsetsMake(5, leftAndRightPaddings, 20, leftAndRightPaddings)
        myCollectionViewLayout.itemSize  = CGSize(width: widthOfCell, height: view.frame.height/3.5)
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.barTintColor = UIColor(colorLiteralRed: 219/255, green: 90/255, blue: 110/255, alpha: 1)
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: (self.navigationController?.navigationBar.frame.height)!))
        titleLabel.text = "SELECT CITY"
        let aS = NSMutableAttributedString(string: "SELECT CITY")
        aS.addAttribute(NSKernAttributeName, value: 5.0, range: NSMakeRange(0, "SELECT CITY".characters.count))
        titleLabel.attributedText = aS
        titleLabel.font = UIFont(name: "Avenir Next", size: 17)
        titleLabel.textColor = UIColor.white
        self.navigationItem.titleView = titleLabel
        
        //configuring first tab bar
        let tabImageUnFilled = UIImage(named: "home-tab-unselected")?.withRenderingMode(.alwaysOriginal)
        let tabImageFilled = UIImage(named: "home-tab-selected")?.withRenderingMode(.alwaysOriginal)
        let tabITem: UITabBarItem = UITabBarItem(title: "Home", image: tabImageUnFilled, selectedImage: tabImageFilled)
        self.tabBarItem = tabITem
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CitiCollectionViewCell
        cell?.layer.masksToBounds = false
        cell?.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell?.layer.shadowRadius = 3;
        cell?.layer.shadowOpacity = 0.5;
        let temp = cities[indexPath.row]
        for (key,_) in temp!{
            cell?.cityImage.image = UIImage(named: key)
            cell?.cityLabel.text = key
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCell = collectionView.cellForItem(at: indexPath) as! CitiCollectionViewCell
        let cityCode = cities[indexPath.row]?[selectedCell.cityLabel.text!]
        tabBarController?.childViewControllers.forEach({ (viewControlller) in
            if let navigationViewController = viewControlller as? UINavigationController{
                if let destinationVc = navigationViewController.childViewControllers[0] as? HotelsViewController {                    tabBarController?.selectedViewController = navigationViewController
                    destinationVc.cityCode = cityCode
                    destinationVc.selectedCity = selectedCell.cityLabel.text
                    
                    DataModal.sharedInstance.selectedCity = selectedCell.cityLabel.text
                    DataModal.sharedInstance.cityCode = cityCode

                    
                }
            }
        })
                //tabBarController?.selectedViewController = destinationVc
    }
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? CitiCollectionViewCell{
            if let destinationVC = segue.destination as? HotelsViewController{
                destinationVC.city = cell.cityLabel.text
            }
        }
    }
    }
    



