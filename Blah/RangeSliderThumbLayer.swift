//
//  RangeSliderThumbLayer.swift
//  Blah
//
//  Created by Jatin Garg on 21/10/16.
//  Copyright © 2016 Jlabs. All rights reserved.
//

import UIKit

class RangeSliderThumbLayer: CALayer {
    var isHighlighted = false{
        didSet{
            setNeedsDisplay()
        }
    }
    weak var rangeSlider: RangeSlider?
    
    override func draw(in ctx: CGContext) {
        guard let slider = rangeSlider
            else{
                return
        }
        let thumbFrame = bounds.insetBy(dx: 2, dy: 2)
        let cornerRadius = thumbFrame.height * slider.curvaceousness / 2
        let thumbPath = UIBezierPath(roundedRect: thumbFrame, cornerRadius: cornerRadius)
        
        //fill with a shadow
        let shadowColor = UIColor.gray
        ctx.setShadow(offset: CGSize(width: 0, height: 1), blur: 1, color: shadowColor.cgColor)
        ctx.setFillColor(slider.thumbTintColor.cgColor)
        ctx.addPath(thumbPath.cgPath)
        ctx.fillPath()
        
        //outline
        ctx.setStrokeColor(shadowColor.cgColor)
        ctx.setLineWidth(0.5)
        ctx.addPath(thumbPath.cgPath)
        ctx.strokePath()
        
        if isHighlighted {
            ctx.setFillColor(HelpingCode().returnAccentColor().withAlphaComponent(1).cgColor)
            ctx.addPath(thumbPath.cgPath)
            ctx.fillPath()
            }
        
    }
}
